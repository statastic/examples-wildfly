package com.example.samplejakarta;

import java.io.*;
import java.security.Principal;

import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/hello-servlet")
//@ServletSecurity(httpMethodConstraints = { @HttpMethodConstraint(value = "GET", rolesAllowed = { "uma_authorization" }) })
public class HelloServlet extends HttpServlet {

    public void init() {

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        Principal user = request.getUserPrincipal();
        out.println(String.format("<h1>Hello %s!</h1>", user != null ? user.getName() : "NO AUTHENTICATED USER"));
        out.println("</body></html>");
    }

    public void destroy() {
    }
}
